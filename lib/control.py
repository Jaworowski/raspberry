"""Control TV module."""
import time
import cec
from .init_module import TimeModule


class TvControl:
    """"Class is responsible for turn on and off TV.

    Manual for hdmi cec:
        https://www.linuxuprising.com/2019/07/raspberry-pi-power-on-off-tv-connected.html
        https://github.com/trainman419/python-cec
    """

    def __init__(self, logger):
        cec.init()
        self.my_cec = cec
        self.tv = cec.Device(cec.CECDEVICE_TV)
        self.log = logger

    def turn_on_screen(self):
        """Send signal to turn on screen using HDMI"""
        # if not self._read_current_status_hdmi_status():
        self.log.info("power_on")
        self.tv.power_on()

        time.sleep(TimeModule.START_UP_TV_S)
        self.log.info("set_active_source")
        self.my_cec.set_active_source(self.tv.address)

    def turn_off_screen(self):
        """Send signal to turn off screen using HDMI"""
        self.log.info("standby. Is active source {}".format(self.my_cec.is_active_source(self.tv.address)))
        if self.my_cec.is_active_source(self.tv.address):
            self.tv.standby()

    # def _read_current_status_hdmi_status(self) -> bool:
    #     """Read if tv is on."""
    #     self.log.info("tv.is_on" + str(self.tv.is_on()))
    #     return self.tv.is_on()


if __name__ == "__main__":
    pass
