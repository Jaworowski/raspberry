"""Sensor module"""
import Adafruit_DHT
from .init_module import PinsNumber


class TempAndHumiditySensor:
    """https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/"""
    DHT_SENSOR = Adafruit_DHT.DHT22

    @staticmethod
    def read_temp_and_humidity() -> str:
        """Function read humidity and temperature.

        :return humidity and temperature as string
        """
        humidity, temperature = Adafruit_DHT.read_retry(TempAndHumiditySensor.DHT_SENSOR, PinsNumber.DHT_PIN)

        if humidity is not None and temperature is not None:
            message: str = "Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity)
        else:
            message: str = "Failed to retrieve data from humidity sensor"

        return message
