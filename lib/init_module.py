"""Dir location"""


class Location:
    PHOTO_FOLDER: str = '/home/pi/my_photo/photos'
    photo_folder_1: str = '/media/pi/sd_photos/temp_1'
    photo_folder_2: str = '/media/pi/sd_photos/temp_2'
    CONFIG_FILE: str = '/home/pi/raspberry/init.ini'
    main_folder: str = '/home/pi/raspberry'
    DATABASE_DIRECTORY: str = '/home/pi/raspberry/database'
    DATABASE_FILE: str = 'my_data'
    LOG_FILE_DIRECTORY: str = '/home/pi/raspberry/log/'
    LOG_FILE: str = LOG_FILE_DIRECTORY + 'log'


class TimeModule:
    motion_sensor_delay_s: int = 1
    photo_delay_s: int = 60
    folder_manager_thread_s: int = 10
    database_search_delay_s: int = 2592000  # 60s * 60m * 24h * 7d
    manage_nas_folder_s: int = 60 * 10
    START_UP_TV_S: int = 16
    WAIT_FOR_TIMEOUT_s: int = 120
    WAIT_FOR_COPY_S: int = 20
    WAIT_FOR_MOUNT_NAS_S: int = 60
    TIME_TO_WAIT_FOR_START_SHOW_SLIDE = 10

    time_to_hold: tuple = (0, 1, 2, 3, 4, 5, 6)


class LogFormat:
    LOG_FORMAT: str = "%(asctime)s - %(levelname)s - %(message)s"


class Command:
    # Power On / Off A TV Connected Via HDMI-CEC
    ON: str = "echo 'on 0.0.0.0' | cec-client -s -d 1"
    ACTIVE_SOURCE: str = "echo 'as 0.0.0.0' | cec-client -s -d 1"
    OFF: str = "echo 'standby 0.0.0.0' | cec-client -s -d 1"

    # HDMI on / off on Raspberry pi
    TURN_OFF_HDMI: str = 'vcgencmd display_power 0'
    TURN_ON_HDMI: str = 'vcgencmd display_power 1'

    # Linux command
    LINUX_COPY_CMD: str = 'cp -r '
    LINUX_DELETE_CMD: str = 'rm -r '
    LINUX_DELETE_FILE: str = 'rm '

    # TODO refactor to separate
    # FEH
    # turn on show slide in selected directory no automatically changing slides
    TURN_ON_SHOW_SLIDE_NO_AUTO: str = "feh -X -Z -F --auto-rotate "
    ADD_TEMP_AND_HUM_INFO: str = "--info "
    FEH: list = ['feh', '-X', '-Z', '-F', '--auto-rotate']
    INFO: str = ' --info '
    MESSAGE: str = 'echo "{}"'

    example = ['feh', '-X', '-Z', '-F', '--auto-rotate', '--info', 'echo "something something"',
               '/home/pi/my_photo/temp_1']
    test = ['feh', '-X', '-Z', '-F', '--auto-rotate', '--info', 'echo "Temp=25.5*C  Humidity=46.0%"',
            '/media/pi/sd_photos/temp_1']


class PinsNumber:
    DHT_PIN: int = 17
    MOTION_SENSOR_PIN: int = 4


class FileExtension:
    photo_extension: tuple = ('jpg', 'jpeg', 'JPG', 'JPEG')
