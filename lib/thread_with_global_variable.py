"""Threading module"""
from os import killpg, system, path
import time
import threading
import signal
from .database import Database
from .sensor_hum_and_temp import TempAndHumiditySensor
from .init_module import Location, Command, PinsNumber, TimeModule
from .nas_control import ManageNasFolder
from gpiozero import MotionSensor
from pynput.keyboard import Key
from pynput.keyboard import Controller
from subprocess import Popen
import logging

# shared between BashCommand and MotionSensorThread
GLOBAL_MOVE: bool = False
GLOBAL_HDMI: bool = True
GLOBAL_NEW_FOLDER_IS_READY: bool = False
GLOBAL_COPY: bool = True

GLOBAL_CURRENT_FOLDER_PATH: str = ""
# GLOBAL_PHOTO_COUNT: int = 0


def check_if_is_hold_time() -> bool:
    """Function return true when is hold time."""

    cur_date = time.ctime()
    cur_time = cur_date.split()[3]
    cur_hour = cur_time.split(":")[0]

    if int(cur_hour) in TimeModule.time_to_hold:
        hold = True
    else:
        hold = False

    return hold


class FolderManagerThread(threading.Thread):
    """Class to manage folders"""

    LOCAL_PHOTO_FOLDER = 'photos'

    def __init__(self, name: str, delay: int, main_logger, is_debug, main_config, main_database, nas: ManageNasFolder):
        """Init FolderManager

        :param str name: Name of thread
        :param int delay: time between run
        :param object main_logger: logger
        :param bool is_debug: True means debug is on
        """
        super().__init__()
        self.is_debug: bool = is_debug
        self.log = main_logger
        self.name: str = name
        self.delay: int = delay
        self.config = main_config
        self.database = main_database

        self.log.info("init folder_manager.py")

        self.my_photo = Location.PHOTO_FOLDER
        self.photo_folder_1 = Location.photo_folder_1
        self.photo_folder_2 = Location.photo_folder_2

        # mounted nas folder as local
        self.external_photo_folder = path.join(self.my_photo)
        self.current_folder = self.photo_folder_1

        self.nas = nas

    def run(self):
        global GLOBAL_COPY
        global GLOBAL_CURRENT_FOLDER_PATH
        global GLOBAL_NEW_FOLDER_IS_READY

        # wait for start slide show before copy new folder
        time.sleep(TimeModule.WAIT_FOR_COPY_S)
        self.log.info("FolderManagerThread after {} s sleep global copy value: {}".format(TimeModule.WAIT_FOR_COPY_S,
                                                                                          GLOBAL_COPY))

        while True:
            if GLOBAL_COPY:
                self.log.debug("FolderManagerThread global_copy")
                self.nas.mount_nas_folder()
                time.sleep(TimeModule.WAIT_FOR_MOUNT_NAS_S)
                self._check_if_is_set_special_next_directory()
                self._swap_current_directory()
                self._update_globals()
                GLOBAL_COPY = False
                self.nas.umont_nas_folder()

            time.sleep(self.delay)

    def _update_globals(self):
        global GLOBAL_CURRENT_FOLDER_PATH
        global GLOBAL_NEW_FOLDER_IS_READY
        GLOBAL_CURRENT_FOLDER_PATH = self._get_current_folder_path()
        GLOBAL_NEW_FOLDER_IS_READY = True

    def _check_if_is_set_special_next_directory(self):
        # global GLOBAL_PHOTO_COUNT

        # check if is set special next directory
        path_from_config: str = self.config.get_directory()

        if path_from_config == "":
            # Copy directory from min usage obtained from database
            path_from_nas = self.database.return_min_used_director()
            self.delete_and_copy_folder(folder_from=path_from_nas)
            # GLOBAL_PHOTO_COUNT = self.database.get_current_folder_photos_number(path_from_nas)
        else:
            self.delete_and_copy_folder(folder_from=path_from_config)
            # GLOBAL_PHOTO_COUNT = self.database.get_current_folder_photos_number(path_from_config)

            self._update_config()

    def _update_config(self):
        self.config.set_directory("")
        self.config.write_config()

    def _get_current_folder_path(self) -> str:
        return self.current_folder

    def _swap_current_directory(self):
        self.log.info("swap_current_directory")
        if self.current_folder == self.photo_folder_1:
            self.current_folder = self.photo_folder_2
        else:
            self.current_folder = self.photo_folder_1

        self.log.debug("FolderManagerThread swap_current_directory current is:" + self.current_folder)

    def delete_and_copy_folder(self, folder_from):
        """Copy folder from remote device to locale drive
        :param folder_from: folder from
        """
        if self.current_folder == self.photo_folder_1:
            self.delete_old_folder(self.photo_folder_2)
            self.copy_folder(folder_from=folder_from, folder_to=self.photo_folder_2)
        else:
            self.delete_old_folder(self.photo_folder_1)
            self.copy_folder(folder_from=folder_from, folder_to=self.photo_folder_1)

    def copy_folder(self, folder_from: str, folder_to: str):
        """Copy folder from ... to ...
        :param folder_from: folder from
        :param folder_to: folder to
        """
        self.log.debug("FolderManagerThread delete_and_copy_folder " + Command.LINUX_COPY_CMD
                       + "'" + folder_from + "' " + folder_to + "'")
        try:
            system(Command.LINUX_COPY_CMD + "'" + folder_from + "' '" + folder_to + "'")
        except Exception as err:
            self.log.error(err)

    def delete_old_folder(self, temp_folder):
        """Delete temporary folder
        :param temp_folder: folder to delete
        """
        self.log.debug("FolderManagerThread delete_old_folder " + temp_folder)
        if path.isdir(temp_folder):
            try:
                system(Command.LINUX_DELETE_CMD + temp_folder)
            except Exception as err:
                self.log.error("delete_old_folder" + str(err))
        else:
            self.log.debug("delete_old_folder -> folder don't exist " + temp_folder)


class BashCommandThread(threading.Thread):
    """This class is responsible for send command to operating system by console"""

    def __init__(self, name: str, delay: int, debug, config, tv_control, logger):
        """:param str name: Name of thread
           :param int delay: time between run
           :param bool debug: True means debug is on
        """
        threading.Thread.__init__(self)

        self.name: str = name
        self.delay: int = delay
        self.is_debug: bool = debug
        self.config = config
        self.log = logger

        self.show_slide_is_on: bool = False

        self.keyboard = Controller()
        self.tv_control = tv_control

        self.display_status: bool = False

        self.photos_in_folder: int = 100

        self.show_slide_process: Popen

    def run(self):
        global GLOBAL_MOVE
        global GLOBAL_HDMI

        global GLOBAL_NEW_FOLDER_IS_READY
        # global GLOBAL_PHOTO_COUNT

        self.log.debug("Starting BashCommandThread")

        # if GLOBAL_PHOTO_COUNT == 0:
        # else:
        #     self.photos_in_folder = GLOBAL_PHOTO_COUNT
        self._turn_on_show_slide_no_auto(Location.photo_folder_1)

        # Main loop
        while True:
            # Next image
            if GLOBAL_MOVE:
                self._check_if_is_next_image()

            # Turn on and off HDMI
            if GLOBAL_HDMI and not self.display_status:
                self.display_status = True
                self.tv_control.turn_on_screen()
            elif not GLOBAL_HDMI and self.display_status:
                self.display_status = False
                self.tv_control.turn_off_screen()

            time.sleep(self.delay)

    def _check_if_is_next_image(self):
        global GLOBAL_NEW_FOLDER_IS_READY
        self.log.debug("_check_if_is_next_image")
        if self.photos_in_folder > 1:
            self.log.debug("next image")
            self.photos_in_folder -= 1
            self._next_image()
        else:
            self.log.debug("else next image")
            if GLOBAL_NEW_FOLDER_IS_READY:
                old_show_slide_popen = self.show_slide_process
                self._turn_on_show_slide_in_new_directory()
                time.sleep(TimeModule.TIME_TO_WAIT_FOR_START_SHOW_SLIDE)
                self._turn_off_show_slide(old_show_slide_popen)

    def _turn_on_show_slide_in_new_directory(self):
        global GLOBAL_COPY
        global GLOBAL_NEW_FOLDER_IS_READY

        self.log.debug("BashCommandThread - Turn on show slide in new directory")
        self.log.debug("BashCommandThread - global_current_folder_path" + GLOBAL_CURRENT_FOLDER_PATH)
        # self.photos_in_folder = GLOBAL_PHOTO_COUNT
        self._turn_on_show_slide_no_auto(GLOBAL_CURRENT_FOLDER_PATH)
        GLOBAL_NEW_FOLDER_IS_READY = False
        GLOBAL_COPY = True

    def _turn_on_show_slide_no_auto(self, current_folder):
        self.photos_in_folder = Database.list_photo_folder(current_folder)
        self.log.debug("In folder {} is photos {}"
                       .format(current_folder, self.photos_in_folder))
        self.show_slide_is_on = True
        self.show_slide_process: Popen = Popen(Command.TURN_ON_SHOW_SLIDE_NO_AUTO + " " + current_folder,
                                               shell=True, start_new_session=True)
        # self.show_slide_process: Popen = Popen(Command.FEH + [current_folder], shell=True, start_new_session=True)
        self.log.info(str(Command.TURN_ON_SHOW_SLIDE_NO_AUTO + " " + current_folder))
        self.log.debug("show_slide_process " + str(self.show_slide_process.pid))

    @staticmethod
    def _prepare_temp_and_hum_msg() -> str:
        msg: str = TempAndHumiditySensor.read_temp_and_humidity()
        return msg

    def _next_image(self):
        self.keyboard.press(Key.space)
        self.keyboard.release(Key.space)

    def _turn_off_show_slide(self, old_show_slide_process: Popen):
        self.log.debug("closing process {}".format(str(old_show_slide_process.pid)))
        killpg(old_show_slide_process.pid, signal.SIGTERM)


class MotionSensorThread(threading.Thread):
    """Thread is responsible only for read movement sensor"""

    def __init__(self, name: str, delay: int, debug, config, logger: logging):
        """Init for Motion Sensor Thread

        :param str name: name
        :param int delay: time between executing
        :param bool debug: True turn on debug mode
        """
        threading.Thread.__init__(self)
        self.pir = MotionSensor(PinsNumber.MOTION_SENSOR_PIN)

        self.name: str = name
        self.delay: int = delay
        self.is_debug: bool = debug
        self.config = config
        self.log: logging = logger

        self.timeout_counter: int = 0

        self._is_move: bool = False
        self._previous_move_status: bool = False

    def run(self):
        global GLOBAL_MOVE
        global GLOBAL_HDMI

        self.log.debug("Starting MotionSensorThread")
        while True:
            # check if MotionSensor return move (true)
            if self.pir.motion_detected and not check_if_is_hold_time():
                self.timeout_counter = 0

                GLOBAL_MOVE = True
                GLOBAL_HDMI = True

            else:
                if self.timeout_counter < TimeModule.WAIT_FOR_TIMEOUT_s:
                    self.timeout_counter += 1
                else:
                    if GLOBAL_MOVE:
                        GLOBAL_MOVE = False
                        GLOBAL_HDMI = False

            time.sleep(self.delay)


if __name__ == "__main__":
    pass
