"""Gui module."""
from tkinter import Tk, Label, Button, W, BooleanVar, Checkbutton, Entry, messagebox, filedialog
import os
from .config import Configurator


class MyGUI:
    """GUI window"""
    def __init__(self, master: Tk, main_config: Configurator):
        """
        :param master: Tk()
        :param main_config: Configurator
        """
        self.master: Tk = master
        self.config: Configurator = main_config
        self.master.title("Settings")
        self._directory_location = None

        self.label = Label(master, text="Options:")

        # IRDA
        self.tv_control_state = BooleanVar()
        self.tv_control_check_button = Checkbutton(master, text='Turn on tv control', var=self.tv_control_state)

        # GUI
        self.gui_state = BooleanVar()
        self.gui_check_button = Checkbutton(master, text="Turn on gui", var=self.gui_state)

        # DEBUG
        self.debug_state = BooleanVar()
        self.debug_check_button = Checkbutton(master, text="Turn on debug", var=self.debug_state)

        # First start
        self.first_state = BooleanVar()
        self.first_start_check_button = Checkbutton(master, text="First start", var=self.first_state)

        # DIRECTORY
        self.label_directory = Label(text="Photos directory")
        self.entry_directory = Entry(bd=1, width=30)
        self.select_directory_button = Button(master, text="Select", command=self.get_directory)

        self.accept_button = Button(master, text="OK", command=self.accept)
        self.close_button = Button(master, text="Close", command=self.reject)
        self.exit_button = Button(master, text="Exit", command=self.master.quit)

        self.set_check_buttons_value()
        self.set_grid()
        self.set_window_geometry()

    def get_directory(self):
        """Read directory from file"""
        directory = filedialog.askdirectory()
        self.entry_directory.delete(0, len(self.entry_directory.get()))
        self.entry_directory.insert(0, directory)

    def set_check_buttons_value(self):
        self.tv_control_state.set(self.config.get_tv_control())
        self.gui_state.set(self.config.get_gui())
        self.debug_state.set(True)

    def set_window_geometry(self):
        """Set window size"""
        self.master.maxsize(640, 250)
        self.master.minsize(600, 160)
        self.master.geometry('600x180')

    def set_grid(self):
        """Prepare window layout."""
        self.label.grid(columnspan=2, sticky=W)

        self.accept_button.grid(row=6, column=1)
        self.close_button.grid(row=6, column=2)
        self.exit_button.grid(row=6, column=4)

        self.tv_control_check_button.grid(row=1, column=1, sticky=W)
        self.gui_check_button.grid(row=2, column=1, sticky=W)
        self.debug_check_button.grid(row=3, column=1, sticky=W)
        self.first_start_check_button.grid(row=4, column=1, sticky=W)

        self.label_directory.grid(row=1, column=4)
        self.entry_directory.grid(row=1, column=5)
        self.select_directory_button.grid(row=2, column=4)

    def reject(self):
        """Reject current changes."""
        self.set_check_buttons_value()
        self.entry_directory.delete(0, len(self.entry_directory.get()))

    def accept(self):
        """Accept current changes."""
        if self.gui_state.get():
            self.config.set_gui_on()
        else:
            self.config.set_gui_off()

        if self.tv_control_state.get():
            self.config.set_tv_control_on()
        else:
            self.config.set_tv_control_off()

        if self.debug_state.get():
            self.config.set_debug_on()
        else:
            self.config.set_debug_off()

        if self.first_state.get():
            self.config.set_first_start()
        else:
            self.config.set_no_first_start()

        if len(self.entry_directory.get()) > 0:
            if os.path.exists(self.entry_directory.get()):
                self._directory_location = self.entry_directory.get()
                self.config.set_directory(self._directory_location)
            else:
                messagebox.showinfo("Error", "Location don't exist")

        self.config.write_config()
        messagebox.showinfo("INFO", "Settings was saved")

    def get_directory_location(self) -> str:
        """Return directory from label."""
        directory = self._directory_location
        self._directory_location = None
        return directory


if __name__ == "__main__":
    pass
