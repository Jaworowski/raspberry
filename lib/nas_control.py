from os import system
from .init_module import Location
import logging


class ManageNasFolder:
    """Class mounting and umonting nas folder"""

    def __init__(self, logger: logging):

        self.nas_folder_mount: bool = True
        self.log = logger

    def mount_nas_folder(self):
        self.log.info("mount_nas_folder")
        system("sudo mount -a")

    def umont_nas_folder(self):
        self.log.info("umont_nas_folder")
        system("sudo umount " + Location.PHOTO_FOLDER)

