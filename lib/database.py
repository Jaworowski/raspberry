"""Database module."""
import os
import shelve
import logging
from .init_module import Location, FileExtension


class Database:
    """Class to operate database"""

    PHOTO: str = 'photo'
    NUMBER: str = 'number'

    def __init__(self, logger: logging, debug: bool, main_photo_folder: str = Location.PHOTO_FOLDER):

        self.log: logging = logger
        self.log.debug("Init database module")
        self.is_debug: bool = debug

        self._db: dict = {}
        self._db_folder_number_of_photos: dict = {}

        self.database_path: str = os.path.join(Location.DATABASE_DIRECTORY, Location.DATABASE_FILE)
        self.log.debug("DatabaseThread path: " + self.database_path)

        if os.path.exists(self.database_path):
            self.open_database()

        self.main_photo_folder: str = main_photo_folder

    def open_database(self):
        """Open database file and read dictionary or create empty dictionary"""
        try:
            with shelve.open(self.database_path) as db_file_open:
                self._db = db_file_open[self.PHOTO]
            self.log.debug('DatabaseThread was read from file: {}'.format(self.database_path))
        except OSError as err:
            self.log.error("Database open error: {}".format(str(err)))
        except KeyError as err_key:
            self.log.error("Problem with KeyError: {}".format(str(err_key)))
        else:
            self._db: dict = {}
            self.log.debug("Created new empty database.")

    def build_database(self, folder_path=Location.PHOTO_FOLDER):
        """Find new folders and add to database (dict) with value 0.

        This function should be called temporary eg. monthly
        """
        self.log.debug("Database build_database start")
        gallery: int = 0

        if os.path.exists(folder_path):
            # go throw the all folders
            for root, _, files in os.walk(folder_path):
                if len(files):
                    self._db.setdefault(root, 0)
                    self._db_folder_number_of_photos[root] = len(files)
                    gallery += len(files)
        else:
            self.log.error("Path don't exist {}".format(folder_path))
        try:
            self._db.pop(self.main_photo_folder)
            self._db_folder_number_of_photos.pop(self.main_photo_folder)
        except KeyError as err:
            self.log.error(str(err))

        self.save_database()

        self.log.debug("Database build_database finished")

    def return_min_used_director(self) -> str:
        """Find the minimal used folder"""

        key_min = min(self._db.keys(), key=(lambda k: self._db[k]))
        self._db[key_min] += 1
        self.save_database()

        self.log.debug("Return min used directory" + key_min)

        return key_min

    def save_database(self):
        """Save database to file"""
        db_file = shelve.open(self.database_path)
        db_file[self.PHOTO] = self._db
        db_file[self.NUMBER] = self._db_folder_number_of_photos

        try:
            db_file.sync()
            db_file.close()
        except Exception as err:
            self.log.error(str(err))

    def print_database(self):
        """Print all database"""
        for key, value in self._db.items():
            if value != 0:
                self.log.debug(key + str(value))

    @staticmethod
    def list_photo_folder(folder_path) -> int:
        """Find new folders and add to database (dict) with value 0.

        This function should be called temporary eg. monthly
        """
        photo: list = []
        for _, _, files in os.walk(folder_path):
            for f in files:
                if f.split('.')[-1] in FileExtension.photo_extension:
                    photo.append(f)
            return len(photo)


if __name__ == "__main__":
    pass
