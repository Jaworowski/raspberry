"""Configurator module."""
import configparser
import os


class Configurator:
    """https://docs.python.org/3/library/configparser.html"""

    def __init__(self, config_file: str):

        self.config_file = config_file
        self.config = configparser.ConfigParser()
        self.config.read(config_file)
        if not self.config.read(config_file):
            self.create_empty_file()

    def get_gui(self) -> bool:
        """Return if gui is set up to run with main program"""

        if self.config.get('gui', 'gui_on') == 'True':
            gui = True
        elif self.config.get('gui', 'gui_on') == 'False':
            gui = False
        else:
            raise ValueError

        return gui

    def set_gui_on(self):
        """Set gui on"""
        self.config.set('gui', 'gui_on', "True")

    def set_gui_off(self):
        """Set gui off"""
        self.config.set('gui', 'gui_on', "False")

    def return_config_file(self) -> str:
        """Return path log file"""
        return self.config.get('log', 'log_file')

    def get_first_start(self):
        """Return first start value.

        :rtype bool
        """
        if self.config.get('start', 'first_start') == 'True':
            first_start = True
        elif self.config.get('start', 'first_start') == 'False':
            first_start = False
        else:
            raise ValueError

        return first_start

    def set_no_first_start(self):
        """Set first start false"""
        self.config.set('start', 'first_start', "False")

    def set_first_start(self):
        """Set first start true"""
        self.config.set('start', 'first_start', "True")

    def get_tv_control(self) -> bool:
        """Return Tv control."""
        if self.config.get('tv', 'control') == 'True':
            tv_control = True
        elif self.config.get('tv', 'control') == 'False':
            tv_control = False
        else:
            raise ValueError

        return tv_control

    def set_tv_control_on(self):
        """Set Tv control true"""
        self.config.set('tv', 'control', "True")

    def set_tv_control_off(self):
        """Set Tv control true"""
        self.config.set('tv', 'control', "False")

    def get_debug(self) -> bool:
        """Return debug option is on."""
        if self.config.get('debug', 'debug_mode') == 'True':
            debug = True
        elif self.config.get('debug', 'debug_mode') == 'False':
            debug = False
        else:
            raise ValueError

        return debug

    def set_debug_on(self):
        """Set debug is on"""
        self.config.set('debug', 'debug_mode', "True")

    def set_debug_off(self):
        """Set debug is off"""
        self.config.set('debug', 'debug_mode', "False")

    def set_directory(self, location: str):
        """Set next directory"""
        self.config.set('directory', 'next_path', location)

    def get_directory(self) -> str:
        """Return next path to show"""
        return self.config.get('directory', 'next_path')

    def write_config(self):
        """Write current config to file."""
        try:
            with open(self.config_file, 'w') as configfile:
                self.config.write(configfile)
        except IOError as err:
            print("Configurator write config error: ", err)

    # This is debug function
    def print_all(self):
        """Print all current options."""
        print("List all contents")
        for section in self.config.sections():
            print("Section: %s" % section)
            for options in self.config.options(section):
                print("x %s:::%s:::%s" % (options,
                                          self.config.get(section, options),
                                          str(type(options))))

    def create_empty_file(self):
        """Crate empty config and write it."""
        self.config['gui'] = {'gui_on': "False"}
        self.config['debug'] = {'debug_mode': "True"}
        self.config['directory'] = {'next_path': ""}
        self.config['start'] = {'first_start': "True"}
        self.config['tv'] = {'control': "True"}
        self.config['log'] = {'log_file': '/home/pi/raspberry/log.log'}
        self.write_config()


# This main is used to test the main function of class
if __name__ == "__main__":
    CFG = Configurator(os.path.join('../', 'init.ini'))
    CFG.print_all()
