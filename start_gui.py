#!/usr/bin/python3
"""Start file GUI"""
import os
from tkinter import Tk
from lib import MyGUI
from lib import Configurator
from lib import Location


if __name__ == "__main__":

    root: Tk = Tk()
    config: Configurator = Configurator(os.path.join(os.getcwd(), Location.CONFIG_FILE))
    my_gui: MyGUI = MyGUI(master=root,
                          main_config=config)

    root.mainloop()
