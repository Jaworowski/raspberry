import os
import glob


def list_photo_folder(folder_path):
    """Find new folders and add to database (dict) with value 0.

    This function should be called temporary eg. monthly
    """
    ex: list = ['pdf']
    photo: list = []
    for _, _, files in os.walk(folder_path):
        for f in files:
            if f.split('.')[-1] in ex:
                photo.append(f)
        return len(photo)


def list_glob(folder_path):
    find = '\*.pdf'
    test = folder_path + find
    print(test)
    a = glob.glob(test)
    print(a)


if __name__ == "__main__":
    number = list_photo_folder(r'C:\Users\mjaworo\Downloads')
    print(number)

    list_glob(r'C:\Users\mjaworo\Downloads')
