#! /usr/bin/env python3
"""Main program."""
import logging
import os
import datetime
import threading
from lib import BashCommandThread
from lib import Configurator
from lib import Database
from lib import FolderManagerThread
from lib import MotionSensorThread
from lib import TvControl
from lib import ManageNasFolder
from lib import TimeModule
from lib import LogFormat
from lib import Location


class Main:
    """Program main function"""

    def __init__(self):

        self.threads: list = []

        self.config: Configurator = Configurator(os.path.join(os.getcwd(), Location.CONFIG_FILE))

        # configuring debug logging
        self.logger: logging = self.preparing_logging()
        self.logger.info('Init main program.')

        self.debug: bool = self.config.get_debug()
        logging.debug("Debug is " + str(self.debug))

        # === No thread objects ===
        self.database: Database = Database(logger=self.logger, debug=self.debug)
        self.control: TvControl = TvControl(self.logger)
        # # = Nas manager =
        self.nas = ManageNasFolder(logger=self.logger)

        # === Initialize thread ===
        # = Bash command =
        self.bash_command: BashCommandThread = BashCommandThread(
            name="BashCommandThread", delay=TimeModule.photo_delay_s, debug=self.debug, config=self.config,
            tv_control=self.control, logger=self.logger)
        self.threads.append(self.bash_command)
        # = Motion Sensor =
        self.motion_sensor: MotionSensorThread = MotionSensorThread(
            name="MotionSensorThread", delay=TimeModule.motion_sensor_delay_s, debug=self.debug,
            config=self.config, logger=self.logger)
        self.threads.append(self.motion_sensor)
        # = FolderManagerThread =
        self.folder_manager: FolderManagerThread = FolderManagerThread(
            name="FolderManagerThread", delay=TimeModule.folder_manager_thread_s, main_logger=self.logger,
            is_debug=self.debug, main_config=self.config, main_database=self.database, nas=self.nas)
        self.threads.append(self.folder_manager)


        self.logger.debug("finish init main program")

    @staticmethod
    def _return_date_to_log() -> str:
        log_time = str(datetime.datetime.now())
        log_time = log_time.replace(" ", "_").replace(":", "_")
        return "_" + log_time

    @staticmethod
    def delete_old_log():
        for the_file in os.listdir(Location.LOG_FILE_DIRECTORY):
            file_path = os.path.join(Location.LOG_FILE_DIRECTORY, the_file)
            try:
                if os.path.isfile(file_path):
                    os.remove(file_path)
                # elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)

    def preparing_logging(self) -> logging:
        """Preparing logging object"""
        self.delete_old_log()
        log_file_name: str = Location.LOG_FILE + Main._return_date_to_log()
        logging.basicConfig(filename=log_file_name,
                            level=logging.DEBUG,
                            format=LogFormat.LOG_FORMAT)
        logger: logging = logging.getLogger('main_program')
        return logger

    def first_start(self):
        # scanning all directory
        self.logger.debug("First start")

        # copy folder from nas to local
        self.folder_manager.delete_old_folder(Location.photo_folder_1)
        self.folder_manager.copy_folder(folder_to=Location.photo_folder_1,
                                        folder_from=self.database.return_min_used_director())
        self.config.set_no_first_start()
        self.config.write_config()

    def no_first_start(self):
        self.logger.debug("No first start")
        if not os.path.exists(Location.photo_folder_1):
            self.folder_manager.copy_folder(folder_to=Location.photo_folder_1,
                                            folder_from=self.database.return_min_used_director())

    def start(self):
        """Start main program."""
        self.logger.info("Starting program")
        self.database.build_database()
        self.database.print_database()

        self.logger.debug("Main init - first start" + str(self.config.get_first_start()))
        if self.config.get_first_start() or not os.path.exists(Location.photo_folder_1):
            self.first_start()
        else:
            self.no_first_start()

        # === Starting threads ===
        for thread in self.threads:  # Starts all the threads.
            thread.start()

        # for thread in self.threads:
        #     thread.join()

        self.logger.debug(self.threads)


if __name__ == "__main__":
    os.chdir(Location.main_folder)
    PROGRAM = Main()
    THREADING_LOCK = threading.Lock()
    PROGRAM.start()
